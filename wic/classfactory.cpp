/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "classfactory_p.h"

ULONG KWicClassFactory::AddRef() {
    m_refCount.ref();
    return m_refCount.load();
}

ULONG KWicClassFactory::Release() {
    m_refCount.deref();
    return m_refCount.load();
}

KWicClassFactory::KWicClassFactory(ObjectConstructor ctor) : m_refCount(1){
    m_ctor = ctor;
}

HRESULT KWicClassFactory::QueryInterface(REFIID riid, void **ppvObject)
{
    if (ppvObject == NULL) {
        return E_INVALIDARG;
    }
    
    *ppvObject = NULL;
    
    if (!IsEqualGUID(riid, IID_IUnknown) && !IsEqualGUID(riid, IID_IClassFactory)) {
        return E_NOINTERFACE;
    }
    
    this->AddRef();
    
    *ppvObject = static_cast<IClassFactory*>(this);
    
    return S_OK;
}

HRESULT KWicClassFactory::CreateInstance(IUnknown* pUnkOuter, REFIID riid, void** ppvObject)
{
    IUnknown* output;
    HRESULT ret;
    
    if (ppvObject == NULL) {
        return E_INVALIDARG;
    }
    
    *ppvObject = NULL;
    
    if (pUnkOuter != NULL) {
        return CLASS_E_NOAGGREGATION;
    }
    
    ret = m_ctor(&output);
    
    if (FAILED(ret)) {
        return ret;
    }
    
    ret = output->QueryInterface(riid, ppvObject);
    
    output->Release();
    
    if (FAILED(ret)) {
        ppvObject = NULL;
    }
    
    return ret;
}

HRESULT KWicClassFactory::LockServer(BOOL fLock)
{
    if (fLock) {
        m_serverLocks.ref();
    } else {
        m_serverLocks.deref();
    }
    return S_OK;
}

int KWicClassFactory::serverLocks() {
    return m_serverLocks.load();
}
