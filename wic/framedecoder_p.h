/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#ifndef KWIC_FRAME_DECODER_H
#define KWIC_FRAME_DECODER_H

#include <QImage>

#include <Wincodec.h>

// Since we're using QImage, we always decode the image as a single frame
class KWicFrameDecoder : public IWICBitmapFrameDecode {
public:
    explicit KWicFrameDecoder(const QImage& image) : m_image(image), m_refCount(1) {}
    ~KWicFrameDecoder() {}
    
    // From IUnknown interface:
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void **ppvObject);
    ULONG STDMETHODCALLTYPE AddRef();
    ULONG STDMETHODCALLTYPE Release();
    
    // From IWICBitmapSource interface:
    virtual HRESULT STDMETHODCALLTYPE GetSize(UINT *puiWidth, UINT *puiHeight);
    virtual HRESULT STDMETHODCALLTYPE GetPixelFormat(WICPixelFormatGUID *pPixelFormat);
    virtual HRESULT STDMETHODCALLTYPE GetResolution(double *pDpiX, double *pDpiY);
    virtual HRESULT STDMETHODCALLTYPE CopyPalette(IWICPalette *pIPalette);
    virtual HRESULT STDMETHODCALLTYPE CopyPixels(const WICRect *prc, UINT cbStride, UINT cbBufferSize, BYTE *pbBuffer);
    
    // From IWICBitmapFrameDecode interface:
    virtual HRESULT STDMETHODCALLTYPE GetMetadataQueryReader(IWICMetadataQueryReader **ppIMetadataQueryReader);
    virtual HRESULT STDMETHODCALLTYPE GetColorContexts(UINT cCount, IWICColorContext **ppIColorContexts, UINT *pcActualCount);
    virtual HRESULT STDMETHODCALLTYPE GetThumbnail(IWICBitmapSource **ppIThumbnail);
private:
    QImage m_image;
    QAtomicInt m_refCount;
};

#endif    // KWIC_FRAME_DECODER_H
