/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "framedecoder_p.h"

#include <Windows.h>
#include <cstdlib>
#include <memory>

HRESULT KWicFrameDecoder::QueryInterface(REFIID riid, void **ppvObject) {
    if (ppvObject == NULL) {
        return E_INVALIDARG;
    }
    
    *ppvObject = NULL;
    
    if (!IsEqualGUID(riid, IID_IUnknown) &&
        !IsEqualGUID(riid, IID_IWICBitmapFrameDecode) &&
        !IsEqualGUID(riid, IID_IWICBitmapSource)) {
        return E_NOINTERFACE;
    }
    
    this->AddRef();
    
    *ppvObject = static_cast<IWICBitmapFrameDecode*>(this);
    
    return S_OK;
}

ULONG KWicFrameDecoder::AddRef() {
    m_refCount.ref();
    return m_refCount.load();
}

ULONG KWicFrameDecoder::Release() {
    m_refCount.deref();
    return m_refCount.load();
}

HRESULT KWicFrameDecoder::GetSize(UINT *puiWidth, UINT *puiHeight) {
    if (m_image.isNull()) {
        return WINCODEC_ERR_WRONGSTATE;
    }

    *puiWidth = m_image.width();
    *puiHeight = m_image.height();
    
    return S_OK;
}

HRESULT KWicFrameDecoder::GetPixelFormat(WICPixelFormatGUID *pPixelFormat) {
    // Map all images into 32bpp RGBA format (QImage::Format_RGBA8888_Premultiplied)
    *pPixelFormat = GUID_WICPixelFormat32bppPRGBA;
    
    return S_OK;
}

HRESULT KWicFrameDecoder::GetResolution(double *pDpiX, double *pDpiY) {

    if (m_image.isNull()) {
        return WINCODEC_ERR_WRONGSTATE;
    }
    
    // This seems broken for some imageformats (e.g.: xcf)
    //*pDpiX = 39.37007874016 * m_image.dotsPerMeterX();
    //*pDpiY = 39.37007874016 * m_image.dotsPerMeterY();
    
    *pDpiX = 96;
    *pDpiY = 96;
    
    return S_OK;
}

HRESULT KWicFrameDecoder::CopyPalette(IWICPalette *pIPalette) {
    if (m_image.isNull()) {
        return WINCODEC_ERR_WRONGSTATE;
    }
    
    // We can't access the palette index from QImage
    return WINCODEC_ERR_PALETTEUNAVAILABLE;
}

HRESULT KWicFrameDecoder::CopyPixels(const WICRect *prc, UINT cbStride, UINT cbBufferSize, BYTE *pbBuffer) {

    if (m_image.isNull()) {
        return E_INVALIDARG;
    }

    if (pbBuffer == NULL) {
        return E_INVALIDARG;
    }

    WICRect rect = {0, 0, m_image.width(), m_image.height()};
    
    if (prc) {
        rect = *prc;
    }
    
    if (rect.Width < 0 || rect.Height < 0 || rect.X < 0 || rect.Y < 0) {
        return E_INVALIDARG;
    }
    
    if ((rect.X + rect.Width > m_image.width()) || (rect.Y + rect.Height > m_image.height())) {
        return E_INVALIDARG;
    }

    if (cbStride / 4 < static_cast<UINT>(rect.Width))
        return E_INVALIDARG;
    if (cbBufferSize / cbStride < static_cast<UINT>(rect.Height))
        return WINCODEC_ERR_INSUFFICIENTBUFFER;

    if (rect.Width == 0 || rect.Height == 0)
        return S_OK;

    QImage imgRGB32 = m_image.convertToFormat(QImage::Format_RGBA8888_Premultiplied);
    
    BYTE* dstBuffer = pbBuffer;
    const int srcStride = imgRGB32.bytesPerLine();
    const int xOffset = rect.X * 4;
    const int width = rect.Width * 4;
    
    for (int srcY = rect.Y; srcY < rect.Y + rect.Height; ++srcY) {
        memcpy(dstBuffer, imgRGB32.scanLine(srcY) + xOffset, width);
        QByteArray k1((char *)dstBuffer, width);
        dstBuffer += cbStride;
    }

  return S_OK;
}

HRESULT KWicFrameDecoder::GetMetadataQueryReader(IWICMetadataQueryReader **ppIMetadataQueryReader) {
    return WINCODEC_ERR_UNSUPPORTEDOPERATION;
}

HRESULT KWicFrameDecoder::GetColorContexts(UINT cCount, IWICColorContext **ppIColorContexts, UINT *pcActualCount) {
    if (m_image.isNull()) {
        return WINCODEC_ERR_BADIMAGE;
    }

    if (pcActualCount == NULL) {
        return E_INVALIDARG;
    }

    *pcActualCount = 0;

    return S_OK;
}

HRESULT KWicFrameDecoder::GetThumbnail(IWICBitmapSource **ppIThumbnail) {
    // no thumbnail by now, though we could generate one
    return WINCODEC_ERR_CODECNOTHUMBNAIL;
}
