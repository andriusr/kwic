/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include <QUuid>

// Class ID of the decoder class for EXR format {51998408-6767-4146-BFFF-742013F7A8EA}
QUuid CLSID_KdeEXRWICDecoder(0x51998408, 0x6767, 0x4146, 0xbf, 0xff, 0x74, 0x20, 0x13, 0xf7, 0xa8, 0xea);
// Class ID of the decoder class for KRA format {97616740-6408-432E-831A-42729BFCB2A2}
QUuid CLSID_KdeKRAWICDecoder(0x97616740, 0x6408, 0x432e, 0x83, 0x1a, 0x42, 0x72, 0x9b, 0xfc, 0xb2, 0xa2);
// Class ID of the decoder class for ORA format {A5627984-31AB-4492-BE13-8EAE847F750E}
QUuid CLSID_KdeORAWICDecoder(0xa5627984, 0x31ab, 0x4492, 0xbe, 0x13, 0x8e, 0xae, 0x84, 0x7f, 0x75, 0xe);
// Class ID of the decoder class for PCX format {8F6CEDE3-46F2-413C-9C21-67520F1B07FD}
QUuid CLSID_KdePCXWICDecoder(0x8f6cede3, 0x46f2, 0x413c, 0x9c, 0x21, 0x67, 0x52, 0xf, 0x1b, 0x7, 0xfd);
// Class ID of the decoder class for PIC format {51FDD0EB-836B-431B-99EA-242E4A4078B4}
QUuid CLSID_KdePICWICDecoder(0x51fdd0eb, 0x836b, 0x431b, 0x99, 0xea, 0x24, 0x2e, 0x4a, 0x40, 0x78, 0xb4);
// Class ID of the decoder class for PSD format {E844CD56-2DD3-4692-9340-ED12953DBD37}
QUuid CLSID_KdePSDWICDecoder(0xe844cd56, 0x2dd3, 0x4692, 0x93, 0x40, 0xed, 0x12, 0x95, 0x3d, 0xbd, 0x37);
// Class ID of the decoder class for RAS format {C7F73D12-A1C8-4293-B424-80A686B51B2A}
QUuid CLSID_KdeRASWICDecoder(0xc7f73d12, 0xa1c8, 0x4293, 0xb4, 0x24, 0x80, 0xa6, 0x86, 0xb5, 0x1b, 0x2a);
// Class ID of the decoder class for RGB format {B50F4707-5533-4335-9ADC-2B3C19B2CA72}
QUuid CLSID_KdeRGBWICDecoder(0xb50f4707, 0x5533, 0x4335, 0x9a, 0xdc, 0x2b, 0x3c, 0x19, 0xb2, 0xca, 0x72);
// Class ID of the decoder class for TGA format {86D58136-AA88-4F6F-AFD9-C8144C5538E9}
QUuid CLSID_KdeTGAWICDecoder(0x86d58136, 0xaa88, 0x4f6f, 0xaf, 0xd9, 0xc8, 0x14, 0x4c, 0x55, 0x38, 0xe9);
// Class ID of the decoder class for XCF format {250EBF43-3CC9-4A48-A104-812351DD338D}
QUuid CLSID_KdeXCFWICDecoder(0x250ebf43, 0x3cc9, 0x4a48, 0xa1, 0x4, 0x81, 0x23, 0x51, 0xdd, 0x33, 0x8d);


// GUID of the EXR container format. {00BECCB5-D821-44C5-8AAB-5147D86CBE5B}
QUuid GUID_ContainerFormatKdeEXR(0xbeccb5, 0xd821, 0x44c5, 0x8a, 0xab, 0x51, 0x47, 0xd8, 0x6c, 0xbe, 0x5b);
// GUID of the KRA container format. {B41E8655-91E4-4751-8013-531D43A68CD7}
QUuid GUID_ContainerFormatKdeKRA(0xb41e8655, 0x91e4, 0x4751, 0x80, 0x13, 0x53, 0x1d, 0x43, 0xa6, 0x8c, 0xd7);
// GUID of the ORA container format. {CF5C007B-CA73-4356-BDF5-15D7FEB55033}
QUuid GUID_ContainerFormatKdeORA(0xcf5c007b, 0xca73, 0x4356, 0xbd, 0xf5, 0x15, 0xd7, 0xfe, 0xb5, 0x50, 0x33);
// GUID of the PCX container format. {5F83D82C-AE26-429B-9BD1-6B1323869CA4}
QUuid GUID_ContainerFormatKdePCX(0x5f83d82c, 0xae26, 0x429b, 0x9b, 0xd1, 0x6b, 0x13, 0x23, 0x86, 0x9c, 0xa4);
// GUID of the PIC container format. {7EB7457C-742C-4238-99B2-4B474D32D31C}
QUuid GUID_ContainerFormatKdePIC(0x7eb7457c, 0x742c, 0x4238, 0x99, 0xb2, 0x4b, 0x47, 0x4d, 0x32, 0xd3, 0x1c);
// GUID of the PSD container format. {AC38052C-34D6-469F-B5F1-8D5B4236CFCE}
QUuid GUID_ContainerFormatKdePSD(0xac38052c, 0x34d6, 0x469f, 0xb5, 0xf1, 0x8d, 0x5b, 0x42, 0x36, 0xcf, 0xce);
// GUID of the RAS container format. {A0F82B2D-5CB9-4DCF-B7BE-F422EEE61DAC}
QUuid GUID_ContainerFormatKdeRAS(0xa0f82b2d, 0x5cb9, 0x4dcf, 0xb7, 0xbe, 0xf4, 0x22, 0xee, 0xe6, 0x1d, 0xac);
// GUID of the RGB container format. {F8C37107-C51D-43B3-9A91-B7D1C8EE3C24}
QUuid GUID_ContainerFormatKdeRGB(0xf8c37107, 0xc51d, 0x43b3, 0x9a, 0x91, 0xb7, 0xd1, 0xc8, 0xee, 0x3c, 0x24);
// GUID of the TGA container format. {9B750E62-478D-42CD-ADF6-11AF1876EF2C}
QUuid GUID_ContainerFormatKdeTGA(0x9b750e62, 0x478d, 0x42cd, 0xad, 0xf6, 0x11, 0xaf, 0x18, 0x76, 0xef, 0x2c);
// GUID of the XCF container format. {C9C05C8E-1AAA-4DAC-8925-1EE9A883A944}
QUuid GUID_ContainerFormatKdeXCF(0xc9c05c8e, 0x1aaa, 0x4dac, 0x89, 0x25, 0x1e, 0xe9, 0xa8, 0x83, 0xa9, 0x44);


#ifdef IMGFORMAT

# define PASTE2(A, B, C) A ## B ## C
# define PASTE(A, B, C) PASTE2(A, B, C)
# define SELECTED_WIC_DECODER PASTE(CLSID_Kde, IMGFORMAT, WICDecoder)
# define SELECTED_WIC_CONTAINER PASTE(CLSID_Kde, IMGFORMAT, WICDecoder)
# define STR2(X) #X
# define STR(X) STR2(X)

QUuid CLSID_KdeWICDecoder(SELECTED_WIC_DECODER);
QUuid GUID_ContainerFormatKde(SELECTED_WIC_CONTAINER);

#endif // IMGFORMAT


// GUID used as the vendor of this DLL.
// {1FAA8E5C-E0AC-4D22-9A31-B11A15C96CC4}
QUuid GUID_KdeCodecVendor(0x1faa8e5c, 0xe0ac, 0x4d22, 0x9a, 0x31, 0xb1, 0x1a, 0x15, 0xc9, 0x6c, 0xc4);

// GUID for IPropertyStore implementation.
// {8C729BCA-0DAA-4044-8046-17FCB2B377E1}
QUuid GUID_KdePropertyStore(0x8c729bca, 0xdaa, 0x4044, 0x80, 0x46, 0x17, 0xfc, 0xb2, 0xb3, 0x77, 0xe1);
