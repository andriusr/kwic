/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "containerdecoder_p.h"
#include "uuids_p.h"

#include <windows.h>
#include <Shlwapi.h>

#include <QBuffer>
#include <QImageReader>

// just to stringify IMGFORMAT
#define STR2(X) #X
#define STR(X) STR2(X)

HRESULT KWicContainerDecoder::QueryInterface(REFIID riid, void** ppvObject) {
    if (ppvObject == Q_NULLPTR) {
        return E_INVALIDARG;
    }
    *ppvObject = Q_NULLPTR;
    
    if (!IsEqualGUID(riid, IID_IUnknown) &&
        !IsEqualGUID(riid, IID_IWICBitmapDecoder)) {
        return E_NOINTERFACE;
    }
        
    this->AddRef();
    
    *ppvObject = static_cast<IWICBitmapDecoder*>(this);
    
    return S_OK;
}

ULONG KWicContainerDecoder::AddRef() {
    m_refCount.ref();
    return m_refCount.load();
}

ULONG KWicContainerDecoder::Release() {
    m_refCount.deref();
    return m_refCount.load();
}

HRESULT checkData(IStream* pIStream, ULARGE_INTEGER* size) {
    HRESULT ret = IStream_Size(pIStream, size);
    if (ret == S_OK) {
        //TODO: Ideally we should construct a QIODevice to encapsulate IStream,
        //      so 'canRead' would only read the necessary bytes
        char* data = new (std::nothrow) char[size->QuadPart];
        ret = IStream_Read(pIStream, data, size->QuadPart);
        if ( ret != S_OK ) {
            delete [] data;
            ret = WINCODEC_ERR_BADIMAGE;
        } else {
            QBuffer* buf = new (std::nothrow) QBuffer;
            buf->setData(data, size->QuadPart);
            buf->open(QIODevice::ReadOnly);
            buf->seek(0);
            QImageReader ir(buf, STR(IMGFORMAT));
            if (ir.canRead()) {
                ret = S_OK;
            } else {
                ret = WINCODEC_ERR_BADIMAGE;
            }
            delete buf;
            delete [] data;
        }
    }
    
    return ret;
}

HRESULT KWicContainerDecoder::QueryCapability(IStream* pIStream, DWORD* pdwCapability) {
    HRESULT ret;
    
    if (pdwCapability == Q_NULLPTR) {
        return E_INVALIDARG;
    }
    
    if (QImageReader::supportedImageFormats().contains(STR(IMGFORMAT))) {
       ULARGE_INTEGER size;
       ret = checkData(pIStream, &size);
    } else {
        ret = E_UNEXPECTED;
    }
    
    if (ret == S_OK) {
        *pdwCapability = WICBitmapDecoderCapabilityCanDecodeSomeImages;
    }
    return ret;
}

HRESULT KWicContainerDecoder::Initialize(IStream* pIStream, WICDecodeOptions cacheOptions) {
    if (pIStream == Q_NULLPTR) {
        return E_INVALIDARG;
    }
    
    HRESULT ret;
    ULARGE_INTEGER fileSize;
    LARGE_INTEGER startPos;
    ULARGE_INTEGER newPos;
    
    ret = checkData(pIStream, &fileSize);
    
    if (FAILED(ret)) {
        return ret;
    }
    
    char* data = new char[fileSize.QuadPart];
    startPos.QuadPart = 0;
    
    ret = pIStream->Seek(startPos, STREAM_SEEK_SET, &newPos);
    if ( ret != S_OK ) {
        delete [] data;
        return WINCODEC_ERR_BADIMAGE;
    }
    
    ret = IStream_Read(pIStream, data, fileSize.QuadPart);
    if ( ret != S_OK ) {
        delete [] data;
        return WINCODEC_ERR_BADIMAGE;
    }
    
    QImage img;
    
    QBuffer* buf = new QBuffer;
    buf->setData(data, fileSize.QuadPart);
    buf->open(QIODevice::ReadOnly);
    buf->seek(0);
    if (!QImageReader(buf, STR(IMGFORMAT)).read(&img)) {
        delete buf;
        delete [] data;
        return WINCODEC_ERR_BADIMAGE;
    }
    
    delete buf;
    delete [] data;
    
    KWicFrameDecoder* newFrame = new KWicFrameDecoder(img);
    
    if ( !m_frame.testAndSetOrdered(Q_NULLPTR, newFrame) ) {
        delete newFrame;
        return WINCODEC_ERR_WRONGSTATE;
    }

    return S_OK;
}

HRESULT KWicContainerDecoder::GetContainerFormat(GUID* pguidContainerFormat) {
    if (pguidContainerFormat == Q_NULLPTR) {
        return E_INVALIDARG;
    }

    *pguidContainerFormat = GUID_ContainerFormatKde;

    return S_OK;
}

HRESULT KWicContainerDecoder::GetDecoderInfo(IWICBitmapDecoderInfo** ppIDecoderInfo) {
    QMutex mutex;
    
    HRESULT result;

    IWICImagingFactory* factory;
    
    {
        QMutexLocker mutexLocker(&mutex);
        
        if (m_factory == Q_NULLPTR) {
            result = CoCreateInstance(CLSID_WICImagingFactory, Q_NULLPTR, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, (LPVOID*) &m_factory);
            if (FAILED(result))
                return result;
        }
        m_factory->AddRef();
        factory = m_factory;
    }
    
    IWICComponentInfo* compInfo;
    compInfo = Q_NULLPTR;
    result = factory->CreateComponentInfo(CLSID_KdeWICDecoder, &compInfo);
    if (FAILED(result))
        return result;
    
    result = compInfo->QueryInterface(IID_IWICBitmapDecoderInfo, (void**)ppIDecoderInfo);
    if (FAILED(result))
        return result;
    
    return S_OK;
}

HRESULT KWicContainerDecoder::CopyPalette(IWICPalette* pIPalette) {
    return WINCODEC_ERR_PALETTEUNAVAILABLE;
}

HRESULT KWicContainerDecoder::GetMetadataQueryReader(IWICMetadataQueryReader** ppIMetadataQueryReader) {
    return WINCODEC_ERR_UNSUPPORTEDOPERATION;
}

HRESULT KWicContainerDecoder::GetPreview(IWICBitmapSource** ppIBitmapSource) {
    return WINCODEC_ERR_UNSUPPORTEDOPERATION;
}

HRESULT KWicContainerDecoder::GetColorContexts(UINT cCount, IWICColorContext** ppIColorContexts, UINT* pcActualCount) {
    return WINCODEC_ERR_UNSUPPORTEDOPERATION;
}

HRESULT KWicContainerDecoder::GetThumbnail(IWICBitmapSource** ppIThumbnail) {
    return WINCODEC_ERR_CODECNOTHUMBNAIL;
}

HRESULT KWicContainerDecoder::GetFrameCount(UINT* pCount) {
    if (pCount == Q_NULLPTR) {
        return E_INVALIDARG;
    }
    
    // We're treating the whole image as a single frame
    *pCount = 1;
    
    return S_OK;
}

HRESULT KWicContainerDecoder::GetFrame(UINT index, IWICBitmapFrameDecode** ppIBitmapFrame) {
    if (ppIBitmapFrame == Q_NULLPTR) {
        return E_INVALIDARG;
    }
    
    if (index != 0) {
        return WINCODEC_ERR_FRAMEMISSING;
    }
    
    QMutex mutex;
    QMutexLocker mutexLocker(&mutex);
    
    if (m_frame == Q_NULLPTR) {
        QImage img;
        *ppIBitmapFrame = new (std::nothrow) KWicFrameDecoder(img);
        if (!*ppIBitmapFrame)
            return E_OUTOFMEMORY;
    } else {
        m_frame.load()->AddRef();
        *ppIBitmapFrame = m_frame.load();
    }
    
    return S_OK;
}
