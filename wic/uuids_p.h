/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#ifndef KDEWICCODEC_UUID_DEFINES_H
#define KDEWICCODEC_UUID_DEFINES_H

#include <QUuid>

#ifdef IMGFORMAT
extern QUuid CLSID_KdeWICDecoder;
extern QUuid GUID_ContainerFormatKde;
#endif // IMGFORMAT

// Class ID of the decoder class for EXR format {51998408-6767-4146-BFFF-742013F7A8EA}
extern QUuid CLSID_KdeEXRWICDecoder;
// Class ID of the decoder class for KRA format {97616740-6408-432E-831A-42729BFCB2A2}
extern QUuid CLSID_KdeKRAWICDecoder;
// Class ID of the decoder class for ORA format {A5627984-31AB-4492-BE13-8EAE847F750E}
extern QUuid CLSID_KdeORAWICDecoder;
// Class ID of the decoder class for PCX format {8F6CEDE3-46F2-413C-9C21-67520F1B07FD}
extern QUuid CLSID_KdePCXWICDecoder;
// Class ID of the decoder class for PIC format {51FDD0EB-836B-431B-99EA-242E4A4078B4}
extern QUuid CLSID_KdePICWICDecoder;
// Class ID of the decoder class for PSD format {E844CD56-2DD3-4692-9340-ED12953DBD37}
extern QUuid CLSID_KdePSDWICDecoder;
// Class ID of the decoder class for RAS format {C7F73D12-A1C8-4293-B424-80A686B51B2A}
extern QUuid CLSID_KdeRASWICDecoder;
// Class ID of the decoder class for RGB format {B50F4707-5533-4335-9ADC-2B3C19B2CA72}
extern QUuid CLSID_KdeRGBWICDecoder;
// Class ID of the decoder class for TGA format {86D58136-AA88-4F6F-AFD9-C8144C5538E9}
extern QUuid CLSID_KdeTGAWICDecoder;
// Class ID of the decoder class for XCF format {250EBF43-3CC9-4A48-A104-812351DD338D}
extern QUuid CLSID_KdeXCFWICDecoder;


// GUID of the EXR container format. {00BECCB5-D821-44C5-8AAB-5147D86CBE5B}
extern QUuid GUID_ContainerFormatKdeEXR;
// GUID of the KRA container format. {B41E8655-91E4-4751-8013-531D43A68CD7}
extern QUuid GUID_ContainerFormatKdeKRA;
// GUID of the ORA container format. {CF5C007B-CA73-4356-BDF5-15D7FEB55033}
extern QUuid GUID_ContainerFormatKdeORA;
// GUID of the PCX container format. {5F83D82C-AE26-429B-9BD1-6B1323869CA4}
extern QUuid GUID_ContainerFormatKdePCX;
// GUID of the PIC container format. {7EB7457C-742C-4238-99B2-4B474D32D31C}
extern QUuid GUID_ContainerFormatKdePIC;
// GUID of the PSD container format. {AC38052C-34D6-469F-B5F1-8D5B4236CFCE}
extern QUuid GUID_ContainerFormatKdePSD;
// GUID of the RAS container format. {A0F82B2D-5CB9-4DCF-B7BE-F422EEE61DAC}
extern QUuid GUID_ContainerFormatKdeRAS;
// GUID of the RGB container format. {F8C37107-C51D-43B3-9A91-B7D1C8EE3C24}
extern QUuid GUID_ContainerFormatKdeRGB;
// GUID of the TGA container format. {9B750E62-478D-42CD-ADF6-11AF1876EF2C}
extern QUuid GUID_ContainerFormatKdeTGA;
// GUID of the XCF container format. {C9C05C8E-1AAA-4DAC-8925-1EE9A883A944}
extern QUuid GUID_ContainerFormatKdeXCF;

// GUID used as the vendor of this DLL.
// {1FAA8E5C-E0AC-4D22-9A31-B11A15C96CC4}
extern QUuid GUID_KdeCodecVendor;

// GUID for IPropertyStore implementation.
// {8C729BCA-0DAA-4044-8046-17FCB2B377E1}
extern QUuid GUID_KdePropertyStore;

#endif // KDEWICCODEC_UUID_DEFINES_H
