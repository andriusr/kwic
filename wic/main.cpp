/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "main_p.h"
#include "containerdecoder_p.h"
#include "classfactory_p.h"
#include "uuids_p.h"

#include <Shlobj.h>

HINSTANCE hSelf;
QAtomicInt nObjects;
QAtomicPointer<KWicClassFactory> factory;

typedef void (STDAPICALLTYPE *SHChangeNotifyFunc)(LONG wEventId, UINT uFlags, LPCVOID dwItem1, LPCVOID dwItem2);

static HRESULT RegisterServer(BOOL fInstall) {
    // TODO: Install registry entries
    
    // Invalidate caches.
    HMODULE hShell32 = LoadLibraryExW(L"shell32.dll", NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
    SHChangeNotifyFunc pSHChangeNotify = (SHChangeNotifyFunc)GetProcAddress(hShell32, "SHChangeNotify");
    if (pSHChangeNotify)
        pSHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
    return S_OK;
}

STDAPI DllRegisterServer() {
    return RegisterServer(TRUE);
}

STDAPI DllUnregisterServer() {
    return RegisterServer(FALSE);
}

STDAPI DllGetClassObject(REFCLSID clsid, REFIID iid, LPVOID *ppv)
{
    if (ppv == NULL) {
        return E_INVALIDARG;
    }
    
    *ppv=NULL;

    if (!IsEqualGUID(iid, IID_IClassFactory)) {
        return E_INVALIDARG;
    }
    
    if (IsEqualGUID(clsid, CLSID_KdeWICDecoder)) {
        factory.testAndSetOrdered(Q_NULLPTR, new (std::nothrow) KWicClassFactory(CreateComObject<KWicContainerDecoder>));
        *ppv = (LPVOID) (factory.load());
        factory.load()->AddRef();
        nObjects.ref();
    } else {
        return CLASS_E_CLASSNOTAVAILABLE;
    }
    
    if (*ppv == NULL) {
        return E_OUTOFMEMORY;
    }
    
    return S_OK;
}

STDAPI DllCanUnloadNow() {
    if (nObjects.load() == 0 && factory.load()->serverLocks() == 0)
        return S_OK;
    else
        return S_FALSE;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
    if (fdwReason == DLL_PROCESS_ATTACH) {
        DisableThreadLibraryCalls(hinstDLL);
        hSelf = hinstDLL;
    }
    
    return TRUE;
}
