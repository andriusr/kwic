/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#ifndef KWIC_CLASS_FACTORY_H
#define KWIC_CLASS_FACTORY_H

#include <QAtomicInt>

#include <Unknwn.h>

// Class factory
typedef HRESULT (*ObjectConstructor)(IUnknown** ppvObject);

// A default constructor. Creates an instance of T. T should be a subclass of
// IUnknown with a parameter-less constructor.
template<typename T>
HRESULT CreateComObject(IUnknown** output) {
    T* result = new (std::nothrow) T();
    if (result == NULL) {
        return E_OUTOFMEMORY;
    }
    *output = static_cast<IUnknown*>(result);
    return S_OK;
}

class KWicClassFactory : public IClassFactory {
public:
    KWicClassFactory(ObjectConstructor ctor);
    
    // from IUnknown interface:
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void **ppvObject);
    ULONG STDMETHODCALLTYPE AddRef();
    ULONG STDMETHODCALLTYPE Release();
    
    // from IClassFactory interface:
    HRESULT STDMETHODCALLTYPE CreateInstance(IUnknown* pUnkOuter, REFIID riid, void** ppvObject);
    HRESULT STDMETHODCALLTYPE LockServer(BOOL fLock);
    
    // other:
    int serverLocks();
    
private:
    QAtomicInt m_refCount;
    QAtomicInt m_serverLocks;
    ObjectConstructor m_ctor;
};

#endif // KWIC_CLASS_FACTORY_H
