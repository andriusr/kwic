/*  This file is part of the KDE project
 *  Copyright (C) 2016 Andrius da Costa Ribas <andriusmao@gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#ifndef KWIC_CONTAINER_DECODER_H
#define KWIC_CONTAINER_DECODER_H

#include "framedecoder_p.h"

#include <Wincodec.h>

// Base decoder class that delegates QImage processing into KWicFrameDecoder
// All supported images are read into QImage and treated as a single frame image
class KWicContainerDecoder : public IWICBitmapDecoder {
public:
    explicit KWicContainerDecoder() : m_refCount(1), m_frame(Q_NULLPTR), m_factory(Q_NULLPTR) {}
    ~KWicContainerDecoder() { delete m_frame; delete m_factory; }
    
    // From IUnknown interface:
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void **ppvObject);
    ULONG STDMETHODCALLTYPE AddRef();
    ULONG STDMETHODCALLTYPE Release();
    
    // From IWICBitmapDecoder interface:
    virtual HRESULT STDMETHODCALLTYPE QueryCapability(IStream *pIStream, DWORD *pdwCapability);
    virtual HRESULT STDMETHODCALLTYPE Initialize(IStream *pIStream, WICDecodeOptions cacheOptions);
    virtual HRESULT STDMETHODCALLTYPE GetContainerFormat(GUID *pguidContainerFormat);
    virtual HRESULT STDMETHODCALLTYPE GetDecoderInfo(IWICBitmapDecoderInfo **ppIDecoderInfo);
    virtual HRESULT STDMETHODCALLTYPE CopyPalette(IWICPalette *pIPalette);
    virtual HRESULT STDMETHODCALLTYPE GetMetadataQueryReader(IWICMetadataQueryReader **ppIMetadataQueryReader);
    virtual HRESULT STDMETHODCALLTYPE GetPreview(IWICBitmapSource **ppIBitmapSource);
    virtual HRESULT STDMETHODCALLTYPE GetColorContexts(UINT cCount, IWICColorContext **ppIColorContexts, UINT *pcActualCount);
    virtual HRESULT STDMETHODCALLTYPE GetThumbnail(IWICBitmapSource **ppIThumbnail);
    virtual HRESULT STDMETHODCALLTYPE GetFrameCount(UINT *pCount);
    virtual HRESULT STDMETHODCALLTYPE GetFrame(UINT index, IWICBitmapFrameDecode **ppIBitmapFrame);
private:
    QAtomicPointer<KWicFrameDecoder> m_frame;
    IWICImagingFactory* m_factory;
    QAtomicInt m_refCount;
};

#endif  // KWIC_CONTAINER_DECODER_H